from typing import List

from fastapi import APIRouter, Body, Depends
from pydantic.types import EmailStr

from app.containers import use_cases
from app.db.database import get_default_bucket
from app.domain.authentication.user_auth import get_current_active_superuser, get_current_active_user
from app.domain.entities.user import User, UserCreate, UserInDB, UserUpdate

router = APIRouter()


@router.get("/", response_model=List[User])
def get_users(
    skip: int = 0,
    limit: int = 100,
    current_user: UserInDB = Depends(get_current_active_superuser),
):
    """
    Retrieve users.
    """
    user_list = use_cases.get_user_list()
    users = user_list(skip=skip, limit=limit)

    return users


@router.get("/search/", response_model=List[User])
def search_users(
    q: str,
    skip: int = 0,
    limit: int = 100,
    current_user: UserInDB = Depends(get_current_active_superuser),
):
    """
    Search users, use Bleve Query String syntax:
    http://blevesearch.com/docs/Query-String-Query/

    For typeahead suffix with `*`. For example, a query with: `email:johnd*` will match
    users with email `johndoe@example.com`, `johndid@example.net`, etc.
    """
    search_user = use_cases.search_user_list()
    users = search_user(query_string=q, skip=skip, limit=limit)
    return users


@router.post("/", response_model=User)
def create_user(
    *,
    user_in: UserCreate,
    current_user: UserInDB = Depends(get_current_active_superuser),
):
    """
    Create new user.
    """
    create_user = use_cases.create_user()
    user = create_user(user_in=user_in)
    return user


@router.put("/me", response_model=User)
def update_user_me(
    *,
    password: str = Body(None),
    full_name: str = Body(None),
    email: EmailStr = Body(None),
    current_user: UserInDB = Depends(get_current_active_user),
):
    """
    Update own user.
    """
    update_own_profile = use_cases.update_own_user()
    user = update_own_profile(password=password,
                              full_name=full_name,
                              email=email,
                              current_user=current_user
                              )
    return user


@router.get("/me", response_model=User)
def read_user_me(current_user: UserInDB = Depends(get_current_active_user)):
    """
    Get current user.
    """
    return current_user


@router.post("/open", response_model=User)
def create_user_open(
    *,
    username: str = Body(...),
    password: str = Body(...),
    email: EmailStr = Body(None),
    full_name: str = Body(None),
):
    """
    Create new user without the need to be logged in.
    """
    register = use_cases.register_user_open()
    user = register(username=username, password=password, email=email, full_name=full_name)
    return user


@router.get("/{username}", response_model=User)
def read_user(username: str, current_user: UserInDB = Depends(get_current_active_user)):
    """
    Get a specific user by username (email).
    """
    get_user = use_cases.get_user()
    user = get_user(username=username, current_user=current_user)
    return user


@router.put("/{username}", response_model=User)
def update_user(
    *,
    username: str,
    user_in: UserUpdate,
    current_user: UserInDB = Depends(get_current_active_superuser),
):
    """
    Update a user.
    """
    update_user = use_cases.update_user()
    user = update_user(username=username, user_in=user_in)
    return user
