from fastapi import APIRouter, Body, Depends
from fastapi.security import OAuth2PasswordRequestForm

from app.containers import use_cases
from app.domain.authentication.user_auth import get_current_active_user
from app.domain.entities.msg import Msg
from app.domain.entities.token import Token
from app.domain.entities.user import User, UserInDB

router = APIRouter()


@router.post("/login/access-token", response_model=Token)
def login(form_data: OAuth2PasswordRequestForm = Depends()):
    """
    OAuth2 compatible token login, get an access token for future requests.
    """
    login_use_case = use_cases.login()
    token = login_use_case(username=form_data.username, password=form_data.password)
    return token


@router.post("/login/test-token", response_model=User)
def test_token(current_user: UserInDB = Depends(get_current_active_user)):
    """
    Test access token.
    """
    return current_user


@router.post("/password-recovery/{username}", response_model=Msg)
def recover_password(username: str):
    """
    Password Recovery.
    """
    recover = use_cases.recover_password()
    recover(username=username)
    return {"msg": "Password recovery email sent"}


@router.post("/reset-password/", response_model=Msg)
def reset_password(
        token: str = Body(...),
        new_password: str = Body(...)):
    """
    Reset password.
    """
    reset = use_cases.reset_password()
    reset(token=token, new_password=new_password)
    return {"msg": "Password updated successfully"}
