from fastapi import APIRouter, Depends

from app.domain.authentication.helpers import ensure_enums_to_strs
from app.domain.authentication.user_auth import get_current_active_superuser
from app.domain.entities.role import RoleEnum, Roles
from app.domain.entities.user import UserInDB

router = APIRouter()


@router.get("/", response_model=Roles)
def read_roles(current_user: UserInDB = Depends(get_current_active_superuser)):
    """
    Retrieve roles.
    """
    roles = ensure_enums_to_strs(RoleEnum)
    return {"roles": roles}
