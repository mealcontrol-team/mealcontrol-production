from enum import Enum, auto


class UseCaseResponseStatusEnum(Enum):
    SUCCESS = auto()
    RESOURCE_ERROR = auto()
    PARAMETERS_ERROR = auto()
    INTERNAL_ERROR = auto()
