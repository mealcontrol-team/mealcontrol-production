from abc import ABC, abstractmethod
from couchbase.bucket import Bucket
from typing import Optional

from app.domain.entities.user import UserUpdate, UserInDB, UserCreate


class IUserDAO(ABC):

    @abstractmethod
    def create(self, bucket: Bucket, *, user_in: UserCreate, persist_to=0):
        pass

    @abstractmethod
    def get(self, bucket: Bucket, *, username: str) -> Optional[UserInDB]:
        pass

    @abstractmethod
    def get_by_email(self, bucket: Bucket, *, email: str) -> Optional[UserInDB]:
        pass

    @abstractmethod
    def update(self, bucket: Bucket, *, username: str,
               user_in: UserUpdate, persist_to=0
               ) -> Optional[UserInDB]:
        pass

    @abstractmethod
    def get_user_list(self, bucket: Bucket, *, skip=0, limit=100):
        pass

    @abstractmethod
    def search(self, bucket: Bucket, *, query_string: str, skip=0, limit=100):
        pass

    @abstractmethod
    def authenticate(self, bucket: Bucket, *, username: str, password: str):
        pass
