from abc import abstractmethod, ABC


class IUseCase(ABC):

    @abstractmethod
    def __call__(self, *args, **kwargs):
        pass
