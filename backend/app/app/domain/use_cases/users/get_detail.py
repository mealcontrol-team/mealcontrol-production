from fastapi import HTTPException

from app.db.database import get_default_bucket
from app.domain.authentication.helpers import is_superuser
from app.domain.interfaces import IUseCase
from app.domain.interfaces.daos import IUserDAO
from app.domain.entities.user import UserInDB


class GetUserUseCase(IUseCase):

    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def __call__(self,
                 username: str,
                 current_user: UserInDB
                 ):
        bucket = get_default_bucket()
        user = self._user_dao.get(bucket, username=username)
        if user == current_user:
            return user
        if not is_superuser(current_user):
            raise HTTPException(
                status_code=400, detail="The user doesn't have enough privileges"
            )
        return user
