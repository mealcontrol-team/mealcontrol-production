from fastapi import HTTPException

from app.db.database import get_default_bucket
from app.domain.interfaces import IUseCase
from app.domain.interfaces.daos import IUserDAO
from app.domain.entities.user import UserUpdate


class UpdateUserUseCase(IUseCase):

    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def __call__(self,
                 username: str,
                 user_in: UserUpdate,
                 ):

        bucket = get_default_bucket()
        user = self._user_dao.get(bucket, username=username)

        if not user:
            raise HTTPException(
                status_code=404,
                detail="The user with this username does not exist in the system",
            )
        user = self._user_dao.update(bucket, username=username, user_in=user_in)
        return user
