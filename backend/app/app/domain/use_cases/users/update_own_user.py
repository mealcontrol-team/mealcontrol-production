from pydantic import EmailStr

from app.db.database import get_default_bucket
from app.domain.interfaces import IUseCase
from app.domain.interfaces.daos import IUserDAO
from app.domain.entities.user import UserUpdate, UserInDB


class UpdateOwnUserUseCase(IUseCase):

    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def __call__(self,
                 password: str,
                 full_name: str,
                 email: EmailStr,
                 current_user: UserInDB,
                 ):

        user_in = UserUpdate(**current_user.dict())
        if password is not None:
            user_in.password = password
        if full_name is not None:
            user_in.full_name = full_name
        if email is not None:
            user_in.email = email
        bucket = get_default_bucket()
        user = self._user_dao.update(bucket, username=current_user.username, user_in=user_in)

        return user
