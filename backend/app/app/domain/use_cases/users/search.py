from typing import List

from app.db.database import get_default_bucket
from app.domain.interfaces import IUseCase
from app.domain.interfaces.daos import IUserDAO
from app.domain.entities.user import User


class SearchUserListUseCase(IUseCase):

    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def __call__(self,
                 query_string: str,
                 skip: int,
                 limit: int,
                 ) -> List[User]:
        bucket = get_default_bucket()
        users = self._user_dao.search(bucket, query_string=query_string, skip=skip, limit=limit)

        return users
