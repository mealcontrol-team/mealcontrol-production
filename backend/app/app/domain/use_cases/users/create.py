from fastapi import HTTPException

from app import config
from app.db.database import get_default_bucket
from app.domain.interfaces import IUseCase
from app.domain.interfaces.daos import IUserDAO
from app.domain.entities.user import UserCreate
from app.utils import send_new_account_email


class CreateUserUseCase(IUseCase):

    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def __call__(self,
                 user_in: UserCreate,
                 ):
        bucket = get_default_bucket()
        user = self._user_dao.get(bucket, username=user_in.username)
        if user:
            raise HTTPException(
                status_code=400,
                detail="The user with this username already exists in the system.",
            )
        user = self._user_dao.create(bucket, user_in=user_in, persist_to=1)

        if config.EMAILS_ENABLED and user.email:
            send_new_account_email(
                email_to=user_in.email, username=user_in.username, password=user_in.password
            )

        return user
