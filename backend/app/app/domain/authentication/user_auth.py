from fastapi import Security, Depends, HTTPException

from app.containers import use_cases
from app.domain.authentication.helpers import is_superuser
from app.domain.authentication.security import reusable_oauth2
from app.domain.entities.user import UserInDB


def get_current_active_user(
        token: str = Security(reusable_oauth2)
) -> UserInDB:
    active_user = use_cases.get_active_user()
    user = active_user(token=token)
    return user


def get_current_active_superuser(
        user: UserInDB = Depends(get_current_active_user)
):
    if not is_superuser(user):
        raise HTTPException(status_code=400, detail="The user doesn't have enough privileges")
    return user
