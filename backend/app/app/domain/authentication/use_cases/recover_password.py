from fastapi import HTTPException

from app.db.database import get_default_bucket
from app.domain.authentication.helpers import generate_password_reset_token, send_reset_password_email
from app.domain.interfaces import IUseCase
from app.domain.interfaces.daos import IUserDAO


class RecoverPasswordUseCase(IUseCase):

    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def __call__(self,
                 username: str
                 ) -> bool:
        bucket = get_default_bucket()
        user = self._user_dao.get(bucket, username=username)

        if not user:
            raise HTTPException(
                status_code=404,
                detail="The user with this username does not exist in the system.",
            )
        password_reset_token = generate_password_reset_token(username=username)
        send_reset_password_email(
            email_to=user.email, username=username, token=password_reset_token
        )
        return True

