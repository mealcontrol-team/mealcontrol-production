from datetime import timedelta

from fastapi import HTTPException

from app import config
from app.db.database import get_default_bucket
from app.domain.authentication.helpers import is_active
from app.domain.authentication.jwt import create_access_token
from app.domain.interfaces import IUseCase
from app.domain.interfaces.daos import IUserDAO
from app.domain.entities.token import Token


class LoginUseCase(IUseCase):

    def __init__(self, user_dao: IUserDAO):
        self._user_dao = user_dao

    def __call__(self,
                 username: str,
                 password: str
                 ) -> Token:

        bucket = get_default_bucket()
        user = self._user_dao.authenticate(
            bucket, username=username, password=password
        )
        if not user:
            raise HTTPException(status_code=400, detail="Incorrect email or password")
        elif not is_active(user):
            raise HTTPException(status_code=400, detail="Inactive user")
        access_token_expires = timedelta(minutes=config.ACCESS_TOKEN_EXPIRE_MINUTES)
        return Token(**{
            "access_token": create_access_token(
                data={"username": user.username}, expires_delta=access_token_expires
            ),
            "token_type": "bearer",
        })
