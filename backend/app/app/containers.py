from dependency_injector import containers, providers

from app.domain.authentication.use_cases.get_active_user import GetActiveUserUseCase
from app.domain.authentication.use_cases.login import LoginUseCase
from app.domain.authentication.use_cases.recover_password import RecoverPasswordUseCase
from app.domain.authentication.use_cases.reset_password import ResetPasswordUseCase
from app.domain.use_cases.users.create import CreateUserUseCase
from app.domain.use_cases.users.get_detail import GetUserUseCase
from app.domain.use_cases.users.get_list import GetUserListUseCase
from app.domain.use_cases.users.register_user_open import RegisterUserOpenUseCase
from app.domain.use_cases.users.search import SearchUserListUseCase
from app.domain.use_cases.users.update_own_user import UpdateOwnUserUseCase
from app.domain.use_cases.users.update_user import UpdateUserUseCase
from app.infrastructure.daos.users import UserDAO


class DataAccessObjects(containers.DeclarativeContainer):
    user = providers.Singleton(UserDAO)


class UseCases(containers.DeclarativeContainer):
    get_active_user = providers.Factory(GetActiveUserUseCase,
                                        DataAccessObjects.user)
    login = providers.Factory(LoginUseCase,
                              DataAccessObjects.user)
    get_user_list = providers.Factory(GetUserListUseCase,
                                      DataAccessObjects.user)
    search_user_list = providers.Factory(SearchUserListUseCase,
                                         DataAccessObjects.user)
    create_user = providers.Factory(CreateUserUseCase,
                                    DataAccessObjects.user)
    update_own_user = providers.Factory(UpdateOwnUserUseCase,
                                        DataAccessObjects.user)
    register_user_open = providers.Factory(RegisterUserOpenUseCase,
                                           DataAccessObjects.user)
    get_user = providers.Factory(GetUserUseCase,
                                 DataAccessObjects.user)
    update_user = providers.Factory(UpdateUserUseCase,
                                    DataAccessObjects.user)
    recover_password = providers.Factory(RecoverPasswordUseCase,
                                         DataAccessObjects.user)
    reset_password = providers.Factory(ResetPasswordUseCase,
                                       DataAccessObjects.user)


use_cases = UseCases
